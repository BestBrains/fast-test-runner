SlaveRunner.lines = SlaveRunner.lines + "\n" + line

if(line.startsWith("//testname:")) {
	SlaveRunner.testName = line;
}

if("//the end".equals(line)) {
  println SlaveRunner.run()
}

import groovy.util.GroovyTestSuite
import junit.framework.Test
import junit.textui.TestRunner
import org.codehaus.groovy.runtime.ScriptTestAdapter
import org.jsoup.Jsoup

class SlaveRunner {

    static lines = ""
		static testName = ""

    static Test createTestSuite(script) {
        def suite = new GroovyTestSuite()

        String[] roots = ['../lib/', '.']
        def engine = new GroovyScriptEngine(roots)
        def testClass = engine.getGroovyClassLoader().parseClass(script)

        suite.addTestSuite(testClass)


        return suite
    }


    static String run() {
        def result = TestRunner.run(SlaveRunner.createTestSuite(lines))
        lines = ""

				def resultText = "{ name: \"${testName}\", runCount:${result.runCount()}, failureCount: ${result.failureCount()}, errorCount: ${result.errorCount()} "
				resultText += ", failures: ["

				if(result.errorCount() > 0 || result.failureCount() > 0) {

					for(error in result.errors()) {
						resultText += "{ type: 'ERROR', description: \"${error.toString()}\"},"
					}
					for(error in result.failures()) {
						resultText += "{ type: 'FAILURE', description: \"${error.toString()}\"},"
					}
				}

				resultText = resultText[0..-1] + "]"
				return resultText + "}"
				
    }

    static void main(String[] args) {
    } 
}
