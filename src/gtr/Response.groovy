package gtr

class Response {

    def response

    Response() {
    }

    Response(resp) {
        response = resp
    }

    def statusCode() {
        return response.statusCode()
    }

    def getHtml() {   
        return new Html(response.parse())
    }
}
