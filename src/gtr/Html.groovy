package gtr

class Html {

    def document
    
    Html() {
    }
    
    Html(doc) {
        document = doc
    }

    def title() {
        return document.title()
    }
    
    def selectSingle(selector) {
        return document.select(selector).first()
    }
}
