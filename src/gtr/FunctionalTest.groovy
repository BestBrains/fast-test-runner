package gtr

import org.jsoup.Connection
import org.jsoup.Jsoup

class FunctionalTest extends GroovyTestCase {
    
    def config
    
    FunctionalTest() {
        config = new ConfigSlurper().parse(new File('config.groovy').toURL())
    }

    def get(url) {
        try {
            def connection = Jsoup.connect(config.baseurl + url).timeout(config.jsoup.request.timeout)
            return new Response(connection.execute())
        } catch (ConnectException ce) {
            throw new Exception("Could not connect to ${config.baseurl}", ce)
        }
    }
    
    def assertThat(item) {
        if(item instanceof Response) {
            return new ResponseAsserter(item)
        }
    }
}
