package gtr

class ResponseAsserter {

    def response

    ResponseAsserter() {
    }

    ResponseAsserter(resp) {
        response = resp
    }

    def isOk() {
        assert response.statusCode() == 200, 'status code is not OK (200)'
    }
}
