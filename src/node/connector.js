var net = require('net');
var fs = require('fs');

var connect = function(host, port, playlist) {

var connectionListener = function() {
		console.log("Test node (%s:%s): CONNECTED", host, port );
	
		if(playlist.hasMore()) 
			pickNextTest();	
	}

function pickNextTest() {	
	var order = playlist.next();
	//console.log("Test %s given to %s:%s", order.name, host, port);
	client.write("//testname: " + order.name + "\n"); 
	client.write(order.script);

	client.write('\n//the end\n');
}

var client = net.connect( {host: host, port: port }, connectionListener );

client.on('data', function(data) {
	var result = eval('(' + data + ')');

	for(var i=0;i<result.failures.length;i++) {
		var failure = result.failures[i];
		console.log(failure.type + ' ' + failure.description);
	}

	console.log("Tests: %s, failures: %s, errors: %s", result.runCount, result.failureCount, result.errorCount);

	if(playlist.hasMore()) {
		pickNextTest();
	}	else {
		//recorder.record(result);
		client.end();
	}

});

client.on('error', function(e) {
	if(e.code == 'ECONNREFUSED') {
		console.log("Test node (%s:%s): OFFLINE", host, port );
	}

});

client.on('end', function() {
	console.log('END');
});

}

exports.connect = connect
