var fs = require('fs');

var allFiles = fs.readdirSync('.');
var index = 0;
var nextFile = '';

var next = function() {

	var order = null;
	

	if(nextFile != '') {
 		nextScript = fs.readFileSync(nextFile);
		order = { name: nextFile, script: nextScript };
		nextFile = '';
	}

	for(var i=index;i<allFiles.length;i++) {
		if(allFiles[i].substr(0,5) == 'test_') {
			nextFile = allFiles[i];
			index = i+1;
			break;
		}
		index = i+1;
	}

	return order;
};

exports.hasMore = function() {
	return nextFile != '';
};

next();

exports.next = next;

