import groovy.ui.GroovySocketServer

class TestSlave {

	static void main(String[] args) {

		def port = 1960

		if(args.size() > 0) {
			port = args[0].toInteger()
		}

		new GroovySocketServer(
        new GroovyShell(),  // evaluator
        true,              // is not file
        "slave/TestRunner.groovy",             // script to return
        true,               // return result to client
        port)               //port

	}
}
