// RunAllScriptsTests.groovy
import groovy.util.GroovyTestSuite
import junit.framework.Test
import junit.textui.TestRunner
import org.codehaus.groovy.runtime.ScriptTestAdapter
import org.jsoup.Jsoup

class AllTests {
    static Test suite() {

        //this.class.classLoader.rootLoader.URLs.each{ println it }
        def suite = new GroovyTestSuite()

        suite.addTestSuite(suite.compile("test.groovy"))
        suite.addTestSuite(suite.compile("test.groovy"))
        suite.addTestSuite(suite.compile("test.groovy"))


        return suite
    }
}

TestRunner.run(AllTests.suite())
