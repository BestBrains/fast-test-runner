package gtr.test

import org.jsoup.Jsoup
import groovy.util.GroovyTestCase
import gtr.FunctionalTest

class simpletest extends FunctionalTest {

    void testHitFrontpage() {
        def response = get("/")
        
        assertThat(response).isOk()
        
        def html = response.getHtml()

        assertEquals("BestBrains", html.title())
        assertEquals("Ignite", html.selectSingle("div.headline").text())
    }
}