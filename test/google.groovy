package gtr.test

import org.jsoup.Jsoup
import groovy.util.GroovyTestCase

class googletest extends FunctionalTest {

    void testHitFrontpage() {
				baseUrl = "http://www.google.com"
        def response = get("/")
        
        assertThat(response).isOk()
        
        def html = response.getHtml()

        assertEquals("Google", html.title())
    }
}
